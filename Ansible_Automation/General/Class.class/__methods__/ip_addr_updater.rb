ipaddr=""
input_ipaddr = $evm.root['dialog_ip_addr']

unless  input_ipaddr.nil? || input_ipaddr.empty?
    ipaddr = input_ipaddr
  else
    $evm.log(:error, "No IP Provided")
end

$evm.log(:info, "This address has been input: #{ipaddr}")

list_values = {
    'required'   => true,
    'protected'  => false,
    'read_only'  => true,
    'value'      => ipaddr,
}
  
list_values.each do |key, value|
    $evm.object[key] = value
end
